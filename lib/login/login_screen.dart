import 'package:cashper/login/login_state.dart';
import 'package:cashper/main.dart';
import 'package:cashper/menu/register_menu.dart';
import 'package:cashper/menu/relation_page.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreen createState() => new _LoginScreen();
}

class _LoginScreen extends State<LoginScreen>{
  var isLogin = false;
  MultiState? ms;
  TextStyle style = TextStyle(fontFamily: 'OpenSans', fontSize: 19.0, color: Colors.black);

  void showToast(state) {
    if (state.warning != null) {
      Fluttertoast.showToast(
          msg: state.warning,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

  goLogin(state) async {
    await state.prosesLogin();
    showToast(state);
    if (state.muncul == true && state.data != null) {
      GetStorage storage = GetStorage();
      storage.write("token", "${state.data!.mToken}");
      Navigator.push(context,
          MaterialPageRoute(
              builder: (context) => RelationPage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    ms = Provider.of<MultiState>(context);
    return ChangeNotifierProvider(
        create: (_) => LoginState(this.ms, context),
    child: Consumer<LoginState>(builder: (context, state, _) {

      final emailField = TextField(
        controller: state.uname,
        obscureText: false,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Username",
            fillColor: Color(0xFFF6E5D0),
            filled: true,
            focusedBorder:OutlineInputBorder(
              borderSide: const BorderSide(color: Color.fromRGBO(255, 159, 29, 1), width: 2.0),
              borderRadius: BorderRadius.circular(32.0),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: const BorderSide(color: Colors.green, width: 2.0),
            )
        ),
      );
      final passwordField = TextField(
        controller: state.pass,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Password",
            fillColor: Color(0xFFF6E5D0),
            filled: true,
            focusedBorder:OutlineInputBorder(
              borderSide: const BorderSide(color: Color.fromRGBO(255, 159, 29, 1), width: 2.0),
              borderRadius: BorderRadius.circular(32.0),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: const BorderSide(color: Colors.green, width: 2.0),
            )
        ),
      );
      final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(255, 159, 29, 1),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () =>  {
          setState(() {
            if (isLogin == false) {
              isLogin = true;
            } else{
              goLogin(state);
            }
          })
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: height * .15),
                  SizedBox(
                    height: 155.0,
                    child: Image.asset("assets/splash/logo.png")
                  ),
                  SizedBox(height: 50.0),
                  emailField,
                  SizedBox(height: 25.0),
                  passwordField,
                  SizedBox(
                    height: 35.0,
                  ),
                  loginButon,
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.centerRight,
                    child: Text('Forgot Password ?',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500)),
                  ),
                  SizedBox(
                    height:  height * .2,
                  ),
                  _createAccountLabel(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
    })
    );
  }

  Widget _createAccountLabel() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => RegisterMenu()));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        padding: EdgeInsets.all(15),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Don\'t have an account ?',
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'Register',
              style: TextStyle(
                  color: Color(0xfff79c4f),
                  fontSize: 13,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }
}



