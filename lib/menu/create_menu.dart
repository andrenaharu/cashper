import 'package:flutter/material.dart';
import 'dart:async';

class CreateRecord extends StatefulWidget {

  @override
  _CreateRecord createState() => new _CreateRecord();
}

class _CreateRecord extends State<CreateRecord> {
  TextStyle style = TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 13,
      color: Color(0xff67727d));
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create"),
        backgroundColor: Color.fromRGBO(255, 159, 29, 1),
      ),
      body: getBody(),
    );
  }

  Widget getBody() {
    String _dropDownValue = "Paid";
    List<String>? _listDropDownValue = [
      "Paid",
      "Income",
    ];
    var size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: DropdownButton<String>(
                onChanged: (newValue) {
                  setState(() {
                    _dropDownValue = newValue!;
                  });
                },
                value: _dropDownValue,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 13,
                    color: Color(0xff67727d)),
                items: _listDropDownValue!.map<DropdownMenuItem<String>>((val) {
                  return DropdownMenuItem<String>(value: val, child: Text(val));
                }).toList()),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.only(
              top: 20, right: 20, left: 20, bottom: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: (size.width - 140),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Date",
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 13,
                                  color: Color(0xff67727d)),
                            ),
                            TextFormField(
                              initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                              cursorColor: Colors.black,
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                              decoration: InputDecoration(
                                  hintText: "Date",
                                  border: InputBorder.none),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        width: 55,
                        height: 50,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 159, 29, 1),
                            borderRadius: BorderRadius.circular(15)),
                        child: ElevatedButton(
                          onPressed: ()=> _selectDate(context),
                          style: ElevatedButton.styleFrom(
                              primary: Color.fromRGBO(255, 159, 29, 1),
                          ),
                          child: Icon(
                            Icons.calendar_today_outlined,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Amount",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 13,
                        color: Color(0xff67727d)),
                  ),
                  TextField(
                    cursorColor: Colors.black,
                    style: TextStyle(
                        fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black),
                    decoration: InputDecoration(
                        hintText: "0.00", border: InputBorder.none),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Memo",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 13,
                        color: Color(0xff67727d)),
                  ),
                  TextField(
                    cursorColor: Colors.black,
                    style: TextStyle(
                        fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black),
                    decoration: InputDecoration(
                        hintText: "Enter Note", border: InputBorder.none),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(10.0),
                    color: Color.fromRGBO(255, 159, 29, 1),
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () =>  {

                      },
                      child: Text("Save",
                          textAlign: TextAlign.center,
                          style: style.copyWith(
                              color: Colors.white, fontWeight: FontWeight.bold)),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}