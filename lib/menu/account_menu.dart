import 'package:cashper/login/login_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class AccountMenu extends StatefulWidget {

  @override
  _AccountMenu createState() => new _AccountMenu();
}

class _AccountMenu extends State<AccountMenu> {
  TextEditingController _email = TextEditingController(text: "anna.hrdynt@gmail.com");
  TextEditingController dateOfBirth = TextEditingController(text: "08-25-1996");
  TextEditingController password = TextEditingController(text: "12345");
  TextEditingController username = TextEditingController(text: "andrenaharu");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Account"),
        backgroundColor: Color.fromRGBO(255, 159, 29, 1),
      ),
      body: getBody(),
    );
  }

  Widget getBody() {
    return SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Card(
                elevation: 10.0,
                // color: Colors.white54.withOpacity(0.01),
                child: ListTile(
                leading: CircleAvatar(
                radius: 30,
                backgroundImage: NetworkImage(
                    "https://images.unsplash.com/photo-1531256456869-ce942a665e80?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTI4fHxwcm9maWxlfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60"),
                ),
                title: Text("Fitriani Rohmah Hardiyanti", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                subtitle: Text("愈究而愈遠 ~ 桜星(◕ᴗ◕✿)", style: TextStyle(fontSize: 15)),
                trailing: Icon(Icons.tag_faces),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              // height: 100,
              child: Card(
                elevation: 10.0,
                child: Padding(
                  padding: const EdgeInsets.only(top:10, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Email",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 13,
                            color: Color(0xff67727d)),
                      ),
                      TextField(
                        controller: _email,
                        cursorColor: Colors.black,
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Email", border: InputBorder.none),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Date of birth",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 13,
                            color: Color(0xff67727d)),
                      ),
                      TextField(
                        controller: dateOfBirth,
                        cursorColor: Colors.black,
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Date of birth", border: InputBorder.none),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Username",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 13,
                            color: Color(0xff67727d)),
                      ),
                      TextField(
                        controller: username,
                        cursorColor: Colors.black,
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Username", border: InputBorder.none),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Password",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 13,
                            color: Color(0xff67727d)),
                      ),
                      TextField(
                        obscureText: true,
                        controller: password,
                        cursorColor: Colors.black,
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Password", border: InputBorder.none),
                      ),
                    ],
                  ),
                ),
              ),
              ),
            SizedBox(
              height: 10,
            ),
            Container(
              // height: 50,
              child: Card(
                elevation: 10.0,
                child: TextButton(
                  onPressed: () {
                    Alert(
                        context: context,
                        title: 'Logout',
                        desc: 'Are you sure you want to log out?',
                        type: AlertType.warning,
                        buttons: [
                          DialogButton(
                            child: Text("Yes", style: TextStyle(fontSize: 19),),
                            onPressed: () async {
                              await GetStorage.init();
                              GetStorage storage = GetStorage();
                              storage.erase();
                              // Navigator.popUntil(context, (route) => route.isFirst);
                              Navigator.push(
                                  context, MaterialPageRoute(builder: (context) => LoginScreen()));
                            },
                            color: Colors.red,
                          ),
                          DialogButton(
                            child: Text("No", style: TextStyle(fontSize: 19),),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: Color.fromRGBO(255, 159, 29, 1),
                          ),
                        ],
                        style: AlertStyle(
                            animationType: AnimationType.fromTop,
                            animationDuration: Duration(milliseconds: 700))).show();
                  },
                  child: ListTile(
                    leading: Icon(Icons.logout),
                    title: Text("Logout", style: TextStyle(fontSize: 17)),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                ),
              ),
            ),
          ],
        ),


    );
  }

}