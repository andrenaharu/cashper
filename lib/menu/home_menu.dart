import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class HomeView extends StatefulWidget {
  @override
  _HomeView createState() => new _HomeView();
}

class _HomeView extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        backgroundColor: Color.fromRGBO(255, 159, 29, 1)
      ),
      body: getBody(),
    );
  }

  Widget getBody() {
    var size = MediaQuery.of(context).size;
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String formatted = formatter.format(now);
    List expenses = [
      {
        "icon": Icons.arrow_back,
        "color": Color(0xFF43aa8b),
        "label": "Income",
        "cost": "Rp. 10,048,000"
      },
      {
        "icon": Icons.arrow_forward,
        "color": Color(0xFFec5766),
        "label": "Paid",
        "cost": "Rp. 3.075,000"
      }
    ];

    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          RichText(text: TextSpan(text: formatted, style: TextStyle(color: Colors.black))),
          SizedBox(
            height: 10,
          ),
          Container(
            child: Card(
              // elevation: 10.0,
              child: ListTile(
                title: Text("Balance of The Month", style: TextStyle(fontWeight: FontWeight.w500,
                    fontSize: 15,
                    color: Color(0xff67727d))),
                subtitle: Text("Rp. 6,973,000", style: TextStyle(fontWeight: FontWeight.bold,
                    fontSize: 20, color: Colors.black), textAlign: TextAlign.right),
              ),
            ),
          ),
          /*Container(
            height: 300,
            child: GridView.builder(
              scrollDirection: Axis.vertical,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 50 / 50,
              ),
              itemBuilder: (buildContext, index) {
                if(index == 0) {
                  return Card(
                    elevation: 10.0,
                    child: ListTile(
                      title: Text("Income",
                          style: TextStyle(fontSize: 20, fontWeight: FontWeight
                              .bold, color: Color(0xFF43aa8b))),
                      subtitle: Text(
                          "Rp9,178,631.83", style: TextStyle(fontSize: 20, color: Color(0xFF43aa8b)),
                          textAlign: TextAlign.right),
                    ),
                  );
                }
                return Card(
                  elevation: 10.0,
                  child: ListTile(
                    title: Text("Paid",
                        style: TextStyle(fontSize: 20, fontWeight: FontWeight
                            .bold, color: Color(0xFFec5766))),
                    subtitle: Text(
                        "Rp9,178,631.83", style: TextStyle(fontSize: 20, color: Color(0xFFec5766)),
                        textAlign: TextAlign.right),
                  ),
                );
              },
              itemCount: 2,
            ),
          )*/
          SizedBox(
            height: 10,
          ),
          Wrap(
              spacing: 20,
              children: List.generate(expenses.length, (index) {
                return Container(
                  width: (size.width - 60) / 2,
                  height: 170,
                  decoration: BoxDecoration(
                      color: Color(0xFFFFFFFF),
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.01),
                          spreadRadius: 10,
                          blurRadius: 3,
                          // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25, top: 20, bottom: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: expenses[index]['color']),
                          child: Center(
                              child: Icon(
                                expenses[index]['icon'],
                                color: Color(0xFFFFFFFF),
                              )),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              expenses[index]['label'],
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 13,
                                  color: Color(0xff67727d)),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              expenses[index]['cost'],
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                );
              }))
        ],
      ),
    );
  }
}
