import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';

class History extends StatefulWidget {

  @override
  _History createState() => new _History();
}

class _History extends State<History> {
  List<String> _listMenuPopUp = ["List", "Chart"];
  List _elements = [
    {'topicName': 'Rp. 10,000,000', 'group': 'Sep-2021', 'typeMon': 'income', 'date': '9/20'},
    {'topicName': 'Rp. 500,000', 'group': 'Sep-2021', 'typeMon': 'expense', 'date': '9/21'},
    {'topicName': 'Rp. 2,500,000', 'group': 'Sep-2021', 'typeMon': 'expense', 'date': '9/25'},
    {'topicName': 'Rp. 48,000', 'group': 'Sep-2021', 'typeMon': 'income', 'date': '9/27'},
    {'topicName': 'Rp. 75,000', 'group': 'Sep-2021', 'typeMon': 'expense', 'date': '9/30'},
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("History"),
        backgroundColor: Color.fromRGBO(255, 159, 29, 1),
        actions: [
          PopupMenuButton(onSelected: (String value) {
            switch (value) {
              case 'List':
                break;
              case 'Chart':
                break;
            }
          }, itemBuilder: (BuildContext _) {
            return _listMenuPopUp.map((e) => PopupMenuItem<String>(value: e, child: Text(e))).toList();
          })
        ],
      ),
      body: GroupedListView<dynamic, String>(
        elements: _elements,
        groupBy: (element) => element['group'],
        groupComparator: (value1,value2) => value2.compareTo(value1),
        itemComparator: (item1, item2) =>
        item1['topicName'].compareTo(item2['topicName']),
        order: GroupedListOrder.DESC,
        // useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) =>
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              value,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 18,
              fontWeight: FontWeight.bold),
            ),
          ),
        itemBuilder: (c, element) {
          Color coll;
          if(element['typeMon']=='expense'){
            coll = Color(0xFFec5766);
            print("merah" + element['typeMon'].toString());
          } else{
            coll = Color(0xFF43aa8b);
            print("ijo" + element['typeMon'].toString());
          }
          return Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0,
                vertical: 6.0),
            child: Container(
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0,
                    vertical: 10.0),

                leading: Text(
                  element['date'],
                  style: TextStyle(fontSize: 16, color: coll),
                ),
                title: Text(
                  element['topicName'],
                  style: TextStyle(fontSize: 16, color: coll),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

}