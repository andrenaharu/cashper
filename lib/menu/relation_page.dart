import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:cashper/menu/account_menu.dart';
import 'package:cashper/menu/create_menu.dart';
import 'package:cashper/menu/history_menu.dart';
import 'package:cashper/menu/home_menu.dart';
import 'package:cashper/menu/setting_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RelationPage extends StatefulWidget{
  @override
  _RelationPageState createState() => _RelationPageState();
}

class _RelationPageState extends State<RelationPage>{
  int pageIndex = 0;
  List<Widget> pages = [
    HomeView(),
    History(),
    SettingMenu(),
    AccountMenu(),
    CreateRecord()
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
          body: getBody(),
          bottomNavigationBar: getFooter(),
          floatingActionButton: FloatingActionButton(
              onPressed: () => setState(() => pageIndex = 4),
              child: Icon(
                Icons.add,
                size: 25,
              ),
              backgroundColor: Color.fromRGBO(255, 159, 29, 1)
            //params
          ),
          floatingActionButtonLocation:
          FloatingActionButtonLocation.centerDocked),
    );
  }

  Future<bool> onWillPop() async{
    final shouldPop = await showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Exit'),
          content: Text('Are you sure you want to exit application?'),
          actions: <Widget>[
            ElevatedButton(
                onPressed: ()=> Navigator.of(context).pop(false),
                child: Text('No'),
                style: ElevatedButton.styleFrom(
                    primary: Color.fromRGBO(255, 159, 29, 1)
                ),
            ),
            ElevatedButton(
                onPressed: () => SystemNavigator.pop(),
                child: Text('Yes'),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red
                ),
            )
          ],
        ),
    );

    return shouldPop ?? false;
  }

  Widget getBody() {
    return IndexedStack(
      index: pageIndex,
      children: pages,
    );
  }

  Widget getFooter() {
    List<IconData> iconItems = [
      Icons.home,
      Icons.list,
      Icons.settings,
      Icons.person,
    ];

    return AnimatedBottomNavigationBar(
      activeColor: Color.fromRGBO(255, 159, 29, 1),
      splashColor: Color.fromRGBO(248, 174, 75, 1),
      inactiveColor: Colors.black.withOpacity(0.5),
      icons: iconItems,
      activeIndex: pageIndex,
      gapLocation: GapLocation.center,
      notchSmoothness: NotchSmoothness.softEdge,
      leftCornerRadius: 10,
      iconSize: 25,
      rightCornerRadius: 10,
      onTap: (index) => setState(() => pageIndex = index),
      //other params
    );

  }

}