import 'package:cashper/login/login_screen.dart';
import 'package:cashper/menu/relation_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';


void main() async{
  await GetStorage.init();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  getPage() {
    GetStorage storage = GetStorage();
    // storage.erase();
    var token = storage.read("token");
    print(token);
    if (token != null) {
      return RelationPage();
    } else {
      return LoginScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(
          create: (c) => MultiState()
      )],
      child: MaterialApp(
        home: Scaffold(
          body: Center(
            child: getPage(),
          ),
        ),
      ),
    );
  }
}

class MultiState extends ChangeNotifier {
  String? token;

  setToken(String value) {
    this.token = value;
    notifyListeners();
  }
}